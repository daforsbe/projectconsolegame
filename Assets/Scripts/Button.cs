﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour
{
	public int casualtyCount = 271;

	public float depth = 0.1f;
	public float pressDuration = 0.3f;
	public AnimationCurve pressed = AnimationCurve.EaseInOut(0, 0, 1, 1);
	public float releaseDuration = 1.0f;
	public AnimationCurve released = AnimationCurve.EaseInOut(0, 1, 1, 0);

	public Light buttonLight;
	public Renderer buttonRenderer;
	public Color disabledColor;
	public AudioSource audioSource;

	public bool tempPressAndRelease = false;
	public bool tempPress = false;
	public bool tempRelease = false;
	
	private bool triggerPressed = false;
	private Vector3 startPos;
	private Color startColor;
	private float startIntensity;

	public void Reset()
	{
		triggerPressed = false;

		if( buttonLight )
		{
			buttonLight.intensity = startIntensity;
		}

		if( buttonRenderer && buttonRenderer.material )
		{
			buttonRenderer.material.color = startColor;
		}
	}

	private void OnButtonPressed()
	{
		if( buttonLight )
		{
			buttonLight.intensity = 0;
		}

		if( buttonRenderer && buttonRenderer.material )
		{
			buttonRenderer.material.color = disabledColor;
		}

		SoundManager.Instance.PlayButtonSound( audioSource );

		ScoreBoard.Instance.AddScore( casualtyCount );
	}

	void Start()
	{
		startPos = transform.localPosition;

		if( buttonRenderer && buttonRenderer.material )
		{
			startColor = buttonRenderer.material.color;
		}

		if( buttonLight )
		{
			startIntensity = buttonLight.intensity;
		}
	}

	void Update()
	{
		if( tempPressAndRelease )
		{
			tempPressAndRelease = false;
			StopAllCoroutines();
			StartCoroutine( PressAndRelease() );
		}
		if( tempPress )
		{
			tempPress = false;
			StopAllCoroutines();
			StartCoroutine( Press() );
		}
		if( tempRelease )
		{
			tempRelease = false;
			StopAllCoroutines();
			StartCoroutine( Release() );
		}
	}

	private void SetDepth( float depth )
	{
		Vector3 newPos = startPos;
		newPos.z -= depth;
		transform.localPosition = newPos;
	}

	private IEnumerator Press()
	{
		if( !triggerPressed )
		{
			triggerPressed = true;
			OnButtonPressed();
		}

		float fCurrentDepth = startPos.z - transform.localPosition.z;
		float fStartValue = pressDuration * ( fCurrentDepth / depth );
		for( float f = fStartValue; f <= pressDuration; f += Time.deltaTime )
		{
			float t = f / pressDuration;
			SetDepth( depth * pressed.Evaluate( t ) );
			yield return null;
		}
		SetDepth( depth * pressed.Evaluate( 1.0f ) );
	}

	private IEnumerator Release()
	{
		for( float f = 0; f <= releaseDuration; f += Time.deltaTime )
		{
			float t = f / releaseDuration;
			SetDepth( depth * released.Evaluate( t ) );
			yield return null;
		}
		SetDepth( depth * released.Evaluate( 1.0f ) );
	}

	private IEnumerator PressAndRelease()
	{
		yield return Press();
		yield return Release();
	}

	void OnTriggerEnter()
	{
		StopAllCoroutines();
		StartCoroutine( PressAndRelease() );
	}
	
}
