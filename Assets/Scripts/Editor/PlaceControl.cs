﻿using UnityEngine;
using UnityEditor;

[CustomEditor( typeof( Control ) )]
public class PlaceControlEditor : Editor
{
	private static bool m_editMode = false;

	void OnSceneGUI()
	{
		Control test = (Control)target;
		if( m_editMode )
		{
			if( Event.current.type == EventType.layout )
			{
				HandleUtility.AddDefaultControl( GUIUtility.GetControlID( GetHashCode(), FocusType.Passive ) );
			}
			if( Event.current.type == EventType.MouseDown )
			{
				Vector2 mousePos = Event.current.mousePosition;
				mousePos.y = Camera.current.pixelHeight - mousePos.y;
				var ray = Camera.current.ScreenPointToRay( mousePos );

				int layerMask = LayerMask.GetMask( "Control" );
				RaycastHit hit;

				if( Physics.Raycast( ray, out hit, 100.0f, layerMask ) )
				{
					test.transform.position = hit.point;
					test.transform.rotation = Quaternion.LookRotation( hit.normal );
				}

				m_editMode = false;
			}
			Event.current.Use();
		}
	}
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

		if( m_editMode )
		{
			if( GUILayout.Button( "Stop", GUILayout.Width( 120 ) ) )
			{
				m_editMode = false;
			}
		}
		else
		{
			if( GUILayout.Button( "Place to Console", GUILayout.Width( 120 ) ) )
			{
				m_editMode = true;
			}
		}
	}
}