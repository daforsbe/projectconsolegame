﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ExplosionManager : MonoBehaviour
{
	public Transform playerParent;
	public Transform buildingsParent;
	public List<GameObject> explosionPrefabs = new List<GameObject>();

	private List<GameObject> buildings = new List<GameObject>();
	private List<GameObject> intactBuildings = new List<GameObject>();

	public static ExplosionManager Instance
	{
		get; private set;
	}

	void Awake()
	{
		if( Instance )
			Debug.LogError( "Multiple ExplosionManagers in the scene" );
		Instance = this;
	}

	void Start()
	{
		for( int i = 0; i < buildingsParent.childCount; ++i )
			buildings.Add( buildingsParent.GetChild( i ).gameObject );
		intactBuildings.AddRange( buildings );
	}
	
	public void ExplodeBuilding()
	{
		if( intactBuildings.Count == 0 || explosionPrefabs.Count == 0 )
			return;
		
		// Search for the nearest intact building
		float minAngle = float.MaxValue;
		int buildingIndex = -1;
		for( int i = 0; i < intactBuildings.Count; ++i )
		{
			var buildingPos = intactBuildings[ i ].transform.position;
			var playerPos = playerParent.position;

			var buildingDir = Vector3.Normalize( buildingPos - playerPos );
			var playerDir = playerParent.forward;

			var angle = Vector3.Angle( playerDir, buildingDir );
			if( angle < minAngle )
			{
				minAngle = angle;
				buildingIndex = i;
			}
		}

		// Randomize the explosion
		int explosionIndex = Random.Range( 0, explosionPrefabs.Count - 1 );
		var explosionPrefab = explosionPrefabs[ explosionIndex ];

		// Explode the building
		var building = intactBuildings[ buildingIndex ];
		intactBuildings.RemoveAt( buildingIndex );
		var explosionGO = Instantiate( explosionPrefab );
		var position =  building.transform.position;
		var dirToPlayer = Vector3.Normalize( playerParent.position - position );
		position += dirToPlayer * 15.0f;
		position += Vector3.up * 0.5f;
		explosionGO.transform.position = position;
		building.GetComponent<Renderer>().material.color = Color.black;
	}

	public void ResetBuildings()
	{
		intactBuildings.Clear();
		intactBuildings.AddRange( buildings );

		foreach( var go in buildings )
			go.GetComponent<Renderer>().material.color = Color.white;
	}
}
