﻿using UnityEngine;
using System.Collections;

public class Gauge : MonoBehaviour
{
	public AnimationCurve curve;
	public float speed = 1.0f;

	private float randomStart = 0.0f;

	void Start()
	{
		randomStart = Random.Range( 0.0f, 1.0f );
		speed = Random.Range( 0.5f, 2.0f );
	}
	
	void Update()
	{
		float localRotation = 45.0f * curve.Evaluate( speed * ( randomStart + Time.time ) );
		transform.localRotation = Quaternion.Euler( new Vector3( 0, 0, localRotation ) );
	}
}
