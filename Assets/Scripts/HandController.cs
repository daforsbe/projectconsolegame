﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HandController : MonoBehaviour
{
	public GameObject[] layouts = new GameObject[ 0 ];
	public GameObject console;
	public Transform endCameraPosition;
	public Image image;
	public AudioSource handAudio;
	public AudioSource laughAudio;
	public AudioClip intro;
	public AudioClip laugh1;
	public AudioClip laugh2;

	public Collider handCollider;
	public float colliderEnableDuration = 0.3f;

	public float distance = 3.1f;
	public float gapStartAngle = 215.0f;
	public float gapEndAngle = 325.0f;
	public Transform handModel;
	public float handOffsetInGap = -4.0f;
	public AnimationCurve handAppearCurve = AnimationCurve.EaseInOut( 0, 1, 1, 0 );
	public AnimationCurve handDisappearCurve = AnimationCurve.EaseInOut( 0, 1, 1, 1 );

	public Transform startPoint;
	public Transform endPoint;
	public Animator handAnimator;

	public bool invertXControls = false;
	public bool invertYControls = false;
	public bool addRandomToControls = false;

	public AnimationCurve randomCurveX = AnimationCurve.EaseInOut( 0, 0, 1, 0 );
	public AnimationCurve randomCurveY = AnimationCurve.EaseInOut( 0, 0, 1, 0 );

	public enum ActionType
	{
		Wait,
		PalmClick
	}

	[System.Serializable]
	public struct Action
	{
		public ActionType type;
		public float duration;
	}
	public List<Action> rhythm = new List<Action>();

	private Vector3 startPos;
	private bool inGap = false;
	private Vector3 targetPos;
	private Vector3 handModelStartPos;
	private int rhythmIndex = 0;
	
	//private List<Button>[] layoutButtons = new List<Button>[ 0 ];
	private bool consoleReseted = true;

	private bool gameEnded = false;
	private int lapIndex = 0;
	
	void Start()
	{
		/*
		layoutButtons = new List<Button>[ layouts.Length ];
		for( int i = 0; i < layouts.Length; ++i )
		{
			layoutButtons[ i ] = new List<Button>();
			layoutButtons[ i ].AddRange( layouts[ i ].GetComponentsInChildren<Button>( true ) );
		}
		*/

		startPos = transform.position;
		handModelStartPos = handModel.localPosition;

		SetHandModelZOffset( handOffsetInGap );
		inGap = true;

		StartCoroutine( RhythmRoutine() );
		ResetConsole();

		handCollider.enabled = false;
	}

	private IEnumerator RhythmRoutine()
	{
		while( true )
		{
			Action action = rhythm[ rhythmIndex ];
			switch( action.type )
			{
				case ActionType.Wait:
					yield return new WaitForSeconds( action.duration );
					break;
				case ActionType.PalmClick:
					handAnimator.SetTrigger( "PalmClick" );

					if( !inGap && !gameEnded )
						handAudio.Play();

					while( !handAnimator.GetCurrentAnimatorStateInfo( 0 ).IsName( "Palm Click" ) )
						yield return null;
					StartCoroutine( EnableCollider() );
					while( handAnimator.GetCurrentAnimatorStateInfo( 0 ).IsName( "Palm Click" ) )
						yield return null;
					
					break;
			}
			rhythmIndex++;
			if( rhythmIndex >= rhythm.Count )
				rhythmIndex = 0;
		}
	}

	private IEnumerator EnableCollider()
	{
		handCollider.enabled = true;
		yield return new WaitForSeconds( colliderEnableDuration );
		handCollider.enabled = false;
	}
	
	void LateUpdate()
	{
		if( gameEnded )
			return;
		
		var startPosInViewport = Camera.main.WorldToViewportPoint( startPoint.position );
		//var endPosInViewport = Camera.main.WorldToViewportPoint( endPoint.position );

		bool startPosVisible = startPosInViewport.z > 0 && startPosInViewport.x >= 0 && startPosInViewport.x <= 1.0f;
		//bool endPosVisible = endPosInViewport.z > 0 && endPosInViewport.x >= 0 && endPosInViewport.x <= 1.0f;

		var viewportPos = new Vector3( 0.5f, 0.2f );
		if( Input.mousePresent )
		{
			float xLimit = 0.20f;
			if( !inGap && startPosVisible )
				xLimit = Mathf.Max( xLimit, startPosInViewport.x );
			
			viewportPos = Camera.main.ScreenToViewportPoint( Input.mousePosition );

			if( invertXControls )
			{
				viewportPos.x = 1.0f - viewportPos.x;
			}

			if( invertYControls )
			{
				viewportPos.y = 0.75f - viewportPos.y;
			}

			if( addRandomToControls )
			{
				viewportPos.x += randomCurveX.Evaluate( Time.time );
				viewportPos.y += randomCurveY.Evaluate( Time.time );
				viewportPos.x -= 3.0f * randomCurveX.Evaluate( 0.2f * Time.time );
				viewportPos.y += 3.0f * randomCurveY.Evaluate( 0.2f * Time.time );
			}

			viewportPos.x = Mathf.Clamp( viewportPos.x, xLimit, 0.80f );
			viewportPos.y = Mathf.Clamp( viewportPos.y, 0.25f, 0.53f );
		}
		
		var ray = Camera.main.ViewportPointToRay( viewportPos );
		Debug.DrawRay( ray.origin, ray.direction * 10.0f, Color.grey );
		
		// Detect if we are pointing out of console
		Quaternion quat = Quaternion.LookRotation( ray.direction );
		float angle = quat.eulerAngles.y;
		while( angle > 360.0f )
			angle -= 360.0f;
		while( angle < 0 )
			angle += 360.0f;
		
		int layerMask = LayerMask.GetMask( "Control" );
		RaycastHit hit;

		float parentRotation = transform.parent.localRotation.eulerAngles.y;
		while( parentRotation > 360.0f )
			parentRotation -= 360.0f;
		while( parentRotation < 0 )
			parentRotation += 360.0f;

		if( Physics.Raycast( ray, out hit, 10.0f, layerMask ) )
		{
			targetPos = hit.point;

			if( inGap && angle >= gapEndAngle )
			{
				inGap = false;
				StartCoroutine( ExitGap() );
			}
		}
		else if( angle > gapStartAngle && angle < Mathf.Lerp( gapStartAngle, gapEndAngle, 0.2f ) )
		{
			if( !inGap )
			{
				inGap = true;
				StartCoroutine( EnterGap() );
			}
		}
		else if( parentRotation > 300.0f )
		{
			if( inGap )
			{
				inGap = false;
				StartCoroutine( ExitGap() );
			}
		}

		if( !inGap )
		{
			Vector3 targetDir = Vector3.Normalize( targetPos - transform.position );
			Quaternion targetRotation = Quaternion.LookRotation( targetDir );
			transform.rotation = targetRotation;

			Vector3 dir = Vector3.Normalize( startPos - targetPos );
			Vector3 targetWorldPos = targetPos + dir * distance;
			transform.position = targetWorldPos;
		}
		
		if( consoleReseted )
		{
			if( transform.parent.localRotation.eulerAngles.y > 300.0f )
			{
				consoleReseted = false;
			}
		}
		else
		{
			if( transform.parent.localRotation.eulerAngles.y > 250.0f &&
				transform.parent.localRotation.eulerAngles.y < 300.0f )
			{
				consoleReseted = true;
				if( lapIndex == 4 )
				{
					StartCoroutine( EndGame() );
					return;
				}

				lapIndex++;
				ResetConsole();
			}
		}
	}

	private void FlipTransform( Transform transform )
	{
		var localScale = transform.localScale;
		localScale.z *= -1;
		transform.localScale = localScale;
	}

	private void ResetConsole()
	{
		for( int i = 0; i < layouts.Length; ++i )
			layouts[ i ].gameObject.SetActive( i == lapIndex );

		if( lapIndex != 0 )
		{
			FlipTransform( console.transform );
		}

		//foreach( var button in layoutButtons[ lapIndex ] )
		//	button.Reset();

		if( lapIndex == 0 )
		{
			laughAudio.PlayDelayed( 13.0f );
		}

		if( lapIndex == 1 )
		{
			Speaker.Instance.PlayControlsIntro();
		}

		if( lapIndex == 2 )
		{
			laughAudio.PlayOneShot( laugh1 );
		}

		if( lapIndex == 3 )
		{
			laughAudio.PlayOneShot( laugh2 );
		}

		if( lapIndex == 4 )
		{
			Speaker.Instance.PlayWorldDestruction();
		}

		if( ExplosionManager.Instance )
			ExplosionManager.Instance.ResetBuildings();
		
		switch( lapIndex )
		{
			case 0: invertXControls = false; invertYControls = false; addRandomToControls = false; break;
			case 1: invertXControls = false; invertYControls = true; addRandomToControls = false; break;
			case 2: invertXControls = true; invertYControls = false; addRandomToControls = false; break;
			case 3: invertXControls = true; invertYControls = true; addRandomToControls = false; break;
			case 4: invertXControls = true; invertYControls = true; addRandomToControls = true; break;
		}
	}

	private void SetAlpha( float f )
	{
		var color = image.color;
		color.a = f;
		image.color = color;
	}

	private IEnumerator EndGame()
	{
		transform.parent.GetComponent<Rotate>().FadeOut();
		EnterGap();

		yield return new WaitForSeconds( 3.0f );

		for( float f = 0; f < 0.5f; f += Time.deltaTime )
		{
			float t = f / 0.5f;
			SetAlpha( t );
			yield return null;
		}
		SetAlpha( 1.0f );
		yield return null;

		Camera.main.transform.position = endCameraPosition.position;
		Camera.main.transform.rotation = endCameraPosition.rotation;

		for( float f = 0; f < 0.5f; f += Time.deltaTime )
		{
			float t = f / 0.5f;
			SetAlpha( 1.0f - t );
			yield return null;
		}
		SetAlpha( 0.0f );
		yield return null;

	}

	private void SetHandModelZOffset( float f )
	{
		var localPos = handModelStartPos;
		localPos.z += f;
		handModel.localPosition = localPos;
	}

	private IEnumerator EnterGap()
	{
		Debug.Log( "EnterGap" );

		float duration = 0.75f;
		for( float f = 0; f < duration; f += Time.deltaTime )
		{
			float t = f / duration;
			SetHandModelZOffset( handOffsetInGap * handDisappearCurve.Evaluate( t ) );
			yield return null;
		}
		SetHandModelZOffset( handOffsetInGap );
		yield return null;
	}

	private IEnumerator ExitGap()
	{
		Debug.Log( "ExitGap" );

		float duration = 0.75f;
		for( float f = 0; f < duration; f += Time.deltaTime )
		{
			float t = f / duration;
			SetHandModelZOffset( handOffsetInGap * handAppearCurve.Evaluate( t ) );
			yield return null;
		}
		SetHandModelZOffset( 0.0f );
		yield return null;
	}

	void OnDrawGizmos()
	{
		Gizmos.color = !inGap ? Color.green : Color.red;
		Gizmos.DrawSphere( targetPos, 0.05f );
	}
}
