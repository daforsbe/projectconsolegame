﻿using UnityEngine;
using System.Collections;

public class LightRotate : MonoBehaviour
{
	public float rotateSpeed = 50.0f;
		
	void Update()
	{
		transform.Rotate( transform.forward, Time.deltaTime * rotateSpeed );
	}
}
