﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Light))]
public class PulsatingLight : MonoBehaviour
{
	public AnimationCurve curve;
	public float intensity = 1.0f;
	public float speed = 1.0f;

	private Light targetLight;

	void Start()
	{
		targetLight = GetComponent<Light>();
	}

	void Update()
	{
		targetLight.intensity = intensity * curve.Evaluate( speed * Time.time );
	}
}
