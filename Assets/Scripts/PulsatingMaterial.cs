﻿using UnityEngine;
using System.Collections;

[RequireComponent( typeof( Renderer ) )]
public class PulsatingMaterial : MonoBehaviour
{
	public AnimationCurve curve;
	public Color color = Color.red;
	public float speed = 1.0f;

	private Renderer targetRenderer;
	private Color startColor;

	void Start()
	{
		targetRenderer = GetComponent<Renderer>();
		startColor = targetRenderer.material.color;
	}

	void Update()
	{
		targetRenderer.material.color = Color.Lerp( startColor, color, curve.Evaluate( speed * Time.time ) );
	}
}
