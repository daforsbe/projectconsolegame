﻿using UnityEngine;
using System.Collections;

public class Rotate : MonoBehaviour
{
	public float startDelay = 10.0f;
	public float fadeOutSpeed = 3.0f;
	public float rotationTime = 20.0f;
	public AnimationCurve fadeIn = AnimationCurve.EaseInOut( 0, 0, 1, 1 );
	public AnimationCurve fadeOut = AnimationCurve.EaseInOut( 0, 1, 1, 0 );

	private float delayTime = 0.0f;
	private float fadeOutTime = 0.0f;
	private float curveTime = 0.0f;
	private bool startFadeOut = false;

	void Update()
	{
		delayTime += Time.deltaTime;
		if( delayTime < startDelay )
			return;

		if( startFadeOut )
			fadeOutTime += Time.deltaTime * fadeOutSpeed;

		float degreesPerSecond = 360.0f / rotationTime;
		transform.Rotate( Vector3.up, Time.deltaTime * degreesPerSecond * fadeIn.Evaluate( curveTime ) * fadeOut.Evaluate( fadeOutTime ) );
		curveTime += Time.deltaTime;
	}

	public void FadeOut()
	{
		startFadeOut = true;
	}
}
