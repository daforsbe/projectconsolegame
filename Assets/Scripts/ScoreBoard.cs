﻿using UnityEngine;
using System.Collections;

public class ScoreBoard : MonoBehaviour
{
	public TextMesh scoreMesh;

	private int casualtyCount = 0;
	private int notVisualizedCasualties = 0;

	public static ScoreBoard Instance
	{
		get; private set;
	}

	void Awake()
	{
		if( Instance )
			Debug.LogError( "Multiple ScoreBoards in the scene" );
		Instance = this;
	}

	public void AddScore( int amount )
	{
		casualtyCount += amount;
		
		scoreMesh.text = casualtyCount.ToString( "D7" );

		notVisualizedCasualties += amount;

		if( ExplosionManager.Instance )
		{
			while( notVisualizedCasualties > 5000 )
			{
				notVisualizedCasualties -= 5000;
				ExplosionManager.Instance.ExplodeBuilding();
			}
		}
	}
}
