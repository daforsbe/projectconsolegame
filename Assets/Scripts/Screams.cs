﻿using UnityEngine;
using System.Collections;

public class Screams : MonoBehaviour
{
	public AnimationCurve curve;
	public float speed = 1.0f;
	public float max = 1.0f;

	private AudioSource target;

	void Start()
	{
		target = GetComponent<AudioSource>();
	}

	void Update()
	{
		target.volume = max * curve.Evaluate( speed * Time.time );
	}
}
