﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{
	public float mainVolume = 1.0f;
	public float effectVolume = 1.0f;
	public float musicVolume = 1.0f;

	public AudioClip buttonSound1;
	public AudioClip buttonSound2;

	public static SoundManager Instance
	{
		get; private set;
	}

	void Awake()
	{
		if( Instance )
			Debug.LogError( "Multiple SoundManagers in the scene" );
		Instance = this;
	}

	public void PlayButtonSound( AudioSource source )
	{
		//PlaySound( source, buttonSound );
		AudioClip clip = Random.Range( 0, 1 ) > 0.5f ? buttonSound1: buttonSound2;
		PlaySound( GetComponent<AudioSource>(), clip );
	}

	private void PlaySound( AudioSource source, AudioClip clip )
	{
		if( source == null || clip == null )
			return;

		source.PlayOneShot( clip, mainVolume * effectVolume );
	}
	
}
