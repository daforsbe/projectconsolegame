﻿using UnityEngine;
using System.Collections;

public class Speaker : MonoBehaviour
{
	public AudioClip introSound;
	public AudioClip radiationLeakSound;
	public AudioClip worldEndSound;

	public static Speaker Instance
	{
		get; private set;
	}

	void Awake()
	{
		if( Instance )
			Debug.LogError( "Multiple SoundManagers in the scene" );
		Instance = this;
	}

	void Start()
	{
		GetComponent<AudioSource>().clip = introSound;
		GetComponent<AudioSource>().PlayDelayed( 1.0f );
	}

	public void PlayControlsIntro()
	{
		if( radiationLeakSound )
		{
			GetComponent<AudioSource>().clip = radiationLeakSound;
			GetComponent<AudioSource>().Play();
		}
	}

	public void PlayWorldDestruction()
	{
		if( worldEndSound )
		{
			GetComponent<AudioSource>().clip = worldEndSound;
			GetComponent<AudioSource>().Play();
		}
	}
}